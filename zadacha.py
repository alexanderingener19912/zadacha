string = "Success"

string = string.lower()

cache = {}

for e in string:
    cache[e] = 0

for e in string:
    cache[e] += 1

result = ""

for e in string:
    if cache[e] > 1:
        result += ')'
    else:
        result += '('

print(result)
